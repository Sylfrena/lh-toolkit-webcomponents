export default{
    "resourceType": "Organization",
    "id": "3142926",
    "meta": {
        "versionId": "1",
        "lastUpdated": "2018-05-02T20:26:49.311+00:00"
    },
    "text": {
        "status": "generated",
        "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\">The Night's Watch</div>"
    },
    "identifier": [
        {
            "system": "www.nationalorgidentifier.gov/ids",
            "value": "Derrick.Lau"
        }
    ],
    "type": [
        {
            "coding": [
                {
                    "system": "http://hl7.org/fhir/organization-type",
                    "code": "prov"
                }
            ]
        }
    ],
    "contact":[{"purpose":"ADMIN"}],
    "name": "The Night's Watch",
    "type" : "ins",
    "address": [
        {
            "line": [
                "1 Kingsroad"
            ],
            "city": "Castle Black",
            "state": "The North",
            "country": "The Seven Kingdoms of Westeros"
        }
    ]
}